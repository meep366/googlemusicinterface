package net.jackcroft.music.view;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.jackcroft.music.User;
import net.jackcroft.music.controller.ControllerMessageType;
import net.jackcroft.music.controller.MusicDatabaseController;

/**
 * Class for providing a command line UI for the interface
 * 
 * @author Jack Croft
 */
public class MusicDatabaseCmdView implements MusicDatabaseView {
	private static Logger logger = LoggerFactory.getLogger(MusicDatabaseCmdView.class);
	private MusicDatabaseController controller;
	private User user;
	private CommandLevel commandLevel = CommandLevel.TOP;
	private Scanner input;
	private boolean rememberInfo;
	private boolean isSorted;

	@Override
	public void show() {
		input = new Scanner(System.in);
		if (controller.getUser() != null) {
			user = controller.getUser();
			enterCommand();
		} else {
			System.out.println("Please input your music directory");
			String musicDirectory = input.nextLine();
			storeInfo();
			user = new User(musicDirectory);
			controller.update(ControllerMessageType.START);
			enterCommand();
		}
	}

	@Override
	public void destroy() {
		input.close();
	}

	@Override
	public void setController(MusicDatabaseController c) {
		controller = c;
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public void login(AccountType accountType, boolean success) {
		if (accountType == AccountType.GOOGLE) {
			googleLogin(success);
		} else if (accountType == AccountType.PLEX) {
			plexLogin(success);
		}
	}

	@Override
	public void command(String msg) {
		System.out.println(msg);
		enterCommand();
	}

	private void enterCommand() {
		boolean done = false;
		while (!done) {
			System.out.println("Please enter a command (type 'help' for a list of commands)");
			String msg = input.nextLine();

			if (commandLevel == CommandLevel.TOP) {
				if (msg.equalsIgnoreCase("help")) {
					System.out.println("Commands: ");
					System.out.println("Help, Use Google, Use Plex, Exit");
				} else if (msg.equalsIgnoreCase("Use Google")) {
					commandLevel = CommandLevel.GOOGLE;
					controller.update(ControllerMessageType.GOOGLE_LOGIN);
				} else if (msg.equalsIgnoreCase("Use Plex")) {
					commandLevel = CommandLevel.PLEX;
					if (user.hasPlexInfo()) {
						controller.update(ControllerMessageType.PLEX_LOGIN);
					} else {
						getPlexInfo();
					}
				} else if (msg.equalsIgnoreCase("Exit")) {
					controller.update(ControllerMessageType.EXIT);
					done = true;
				} else {
					System.out.println("Invalid Command");
				}
			} else if (commandLevel == CommandLevel.GOOGLE) {
				if (msg.equalsIgnoreCase("help")) {
					System.out.println("Commands: ");
					System.out.println("Help, Create iTunes Playlists, Exit");
				} else if (msg.equalsIgnoreCase("Create iTunes Playlists")) {
					controller.update(ControllerMessageType.ITUNES_PLAYLISTS_FROM_GOOGLE);
					done = true;
				} else if (msg.equalsIgnoreCase("Exit")) {
					commandLevel = CommandLevel.TOP;
				} else {
					System.out.println("Invalid Command");
				}
			} else if (commandLevel == CommandLevel.PLEX) {
				if (msg.equalsIgnoreCase("help")) {
					System.out.println("Commands: ");
					System.out.println("Help, Create iTunes Playlists, Create Plex Playlist, Exit");
				} else if (msg.equalsIgnoreCase("Create iTunes Playlists")) {
					controller.update(ControllerMessageType.ITUNES_PLAYLISTS_FROM_PLEX);
					done = true;
				} else if (msg.equalsIgnoreCase("Create Plex Playlist")) {
					System.out.println("Please input the location of the playlist");
					String playlistFile = input.nextLine();
					user.setPlexPlaylistFile(playlistFile);
					System.out.println("Sort playlist by artist? Y/N");
					boolean inputComplete = false;
					while (!inputComplete) {
						String sortByArtist = input.nextLine();
						if (sortByArtist.equalsIgnoreCase("y")) {
							isSorted = true;
							inputComplete = true;
						} else if (sortByArtist.equalsIgnoreCase("n")) {
							isSorted = false;
							inputComplete = true;
						} else
							System.out.println("Invalid input");
					}

					controller.update(ControllerMessageType.PLEX_PLAYLIST_FROM_ITUNES);
					done = true;
				} else if (msg.equalsIgnoreCase("Exit")) {
					commandLevel = CommandLevel.TOP;
				} else {
					System.out.println("Invalid Command");
				}
			}
		}
	}

	private void googleLogin(boolean success) {
		if (success) {
			System.out.println("Login Successful");
		} else {
			System.out.println("Login incorrect, please create new OAuth token");
			commandLevel = CommandLevel.TOP;
		}
	}

	private void plexLogin(boolean success) {
		if (success) {
			System.out.println("Login Successful");
		} else {
			System.out.println("Email or password incorrect");
			getPlexInfo();
		}
	}

	private void getPlexInfo() {
		System.out.println("Please input your username");
		String username = input.nextLine();
		System.out.println("Please input your password");
		String password = input.nextLine();
		storeInfo();
		user = new User(user.getDirectory(), username, password);
		controller.update(ControllerMessageType.PLEX_LOGIN);
	}

	private void storeInfo() {
		boolean done = false;
		while (!done) {
			System.out.println("Would you like your information remebered? Y/N");
			String remember = input.nextLine();
			if (remember.equalsIgnoreCase("y")) {
				rememberInfo = true;
				done = true;
			} else if (remember.equalsIgnoreCase("n")) {
				rememberInfo = false;
				done = true;
			} else
				System.out.println("Invalid input");
		}
	}

	@Override
	public boolean getRememberInfo() {
		return rememberInfo;
	}

	@Override
	public boolean isSorted() {
		return isSorted;
	}
}