package net.jackcroft.music.view;

/**
 * Interface to define basic view methods
 * 
 * @author Jack Croft
 * 
 */
public interface View {

	/**
	 * Displays the View
	 */
	public void show();

	/**
	 * Destroys the View
	 */
	public void destroy();

}