package net.jackcroft.music.view;

import net.jackcroft.music.User;
import net.jackcroft.music.controller.MusicDatabaseController;

/**
 * Interface for defining methods to be implemented by any MusicDatabase view,
 * command line or not
 * 
 * @author Jack Croft
 * 
 */
public interface MusicDatabaseView extends View {
	/**
	 * Sets the controller for the view
	 * 
	 * @param c
	 *            The controller to be set
	 */
	public void setController(MusicDatabaseController c);

	/**
	 * Returns the user that was inputed
	 * 
	 * @return The inputed user
	 */
	public User getUser();

	/**
	 * Method returning the success of the login attempt.
	 * 
	 * @param accountType
	 *            The type of account that was logged in to
	 * @param success
	 *            Whether or not the login succeeded
	 */
	public void login(AccountType accountType, boolean success);

	/**
	 * Method called after the execution of a command with a message to display
	 * to the user
	 * 
	 * @param msg
	 *            The message to be displayed
	 */
	public void command(String msg);

	/**
	 * Returns true if the user wants info remembered
	 * 
	 * @return Whether or not the user wants their info remembered
	 */
	public boolean getRememberInfo();
	
	/**
	 * Returns true if the user wants the playlist sorted
	 * 
	 * @return Whether or not the user wants the playlist sorted
	 */
	public boolean isSorted();
}