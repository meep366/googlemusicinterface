package net.jackcroft.music;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to read in files from a given directory and create a MusicDatabase
 * 
 * @author Jack Croft
 */
public class MusicFileScanner extends Thread {

	private static Logger logger = LoggerFactory.getLogger(MusicFileScanner.class);
	private String directory;
	private Map<Song, File> songMap;
	private MusicDatabase database;
	private boolean done;
	private long tagTime;

	/**
	 * Constructor requiring a directory to search
	 * 
	 * @param d
	 *            the directory to search
	 */
	public MusicFileScanner(String d) {
		songMap = new HashMap<Song, File>();
		directory = d;
		done = false;
	}

	@Override
	public void run() {
		findSongFiles(directory);
		List<Song> songs = new ArrayList<Song>(songMap.keySet());
		database = new MusicDatabase(songs);

		done = true;
	}

	/**
	 * Method to return whether the class is done reading the local music data
	 * 
	 * @return the completion status of the run method
	 */
	public boolean isDone() {
		return done;
	}

	/**
	 * Returns the music database that this class creates upon running
	 * 
	 * @return the music database for the local drive
	 */
	public MusicDatabase getDatabase() {
		return database;
	}

	/**
	 * Method to find all song files in a given directory
	 * 
	 * @param startingDirectory
	 *            the directory to search
	 * @return the map of songs to files
	 */
	private List<File> findSongFiles(String startingDirectory) {
		logger.info("Finding Song Files");
		long startTime = System.nanoTime();
		List<File> songMap = new LinkedList<File>();

		recursiveFindSongFiles(startingDirectory, songMap);
		logger.debug("Tag Time: " + (tagTime) / 1000000000.);
		logger.debug("Find Song Time: " + (System.nanoTime() - startTime) / 1000000000.);
		return songMap;
	}

	/**
	 * a helper method to find a lists all files recursively in a given
	 * directory
	 * 
	 * @param startingDirectory
	 *            the directory to find all files in
	 */
	private void recursiveFindSongFiles(String startingDirectory, List<File> fileList) {
		logger.trace("Finding files in: " + startingDirectory);
		File[] files = listFiles(startingDirectory);
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory())
				recursiveFindSongFiles(files[i].getPath(), fileList);
			else {
				if (files[i].getPath().contains(".mp3") || files[i].getPath().contains(".m4a")) {
					try {
						long startTime = System.nanoTime();
						Tag t = AudioFileIO.read(files[i]).getTag();
						tagTime += System.nanoTime() - startTime;
						Map<String, String> attributes = new HashMap<String, String>();
						String[] keys = { "Title", "Album", "Comment", "Composer", "Disk Number", "Beats Per Minute",
								"Artist", "Rating", "Total Disk Count", "Album Artist", "Genre" };

						String[] values = new String[11];
						values[0] = t.getFirst(FieldKey.TITLE);
						values[1] = t.getFirst(FieldKey.ALBUM);
						values[2] = t.getFirst(FieldKey.COMMENT);
						values[3] = t.getFirst(FieldKey.COMPOSER);
						values[4] = t.getFirst(FieldKey.DISC_NO);
						values[5] = t.getFirst(FieldKey.BPM);
						values[6] = t.getFirst(FieldKey.ARTIST);
						values[7] = t.getFirst(FieldKey.RATING);
						values[8] = t.getFirst(FieldKey.DISC_TOTAL);
						values[9] = t.getFirst(FieldKey.ALBUM_ARTIST);
						values[10] = t.getFirst(FieldKey.GENRE);
						for (int j = 0; j < values.length; j++) {
							if (!(values[j].equals(""))) {
								attributes.put(keys[j], values[j]);
							}
						}
						Song s = new Song(attributes, files[i]);
						songMap.put(s, files[i]);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * helper method to list all files in a given directory
	 * 
	 * @param directory
	 *            the directory to have its files listed
	 * @return the list of files in the directory
	 */
	private File[] listFiles(String directory) {
		logger.trace("Listing file in: " + directory);
		File dir = new File(directory);
		return dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return filename.endsWith("");
			}
		});
	}
}