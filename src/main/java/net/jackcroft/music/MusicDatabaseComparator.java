package net.jackcroft.music;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for comparing two different MusicDatabases in various ways
 * 
 * @author Jack Croft
 */
public class MusicDatabaseComparator {

	private static Logger logger = LoggerFactory.getLogger(MusicDatabaseComparator.class);

	/**
	 * Method for comparing two databases and matching their songs and files,
	 * and sets location for all remote songs
	 * 
	 * @param remoteData
	 *            the songs from the remote database
	 * @param localData
	 *            the songs from a local drive
	 */
	public void matchSongs(MusicDatabase remoteData, MusicDatabase localData) {
		List<Song> remoteSongs = remoteData.getSongs();
		List<Song> localSongList = localData.getSongs();
		Map<Song, Song> localSongs = new HashMap<Song, Song>();

		for (int i = 0; i < localSongList.size(); i++) {
			localSongs.put(localSongList.get(i), localSongList.get(i));
		}

		for (int i = 0; i < remoteSongs.size(); i++) {
			if (localSongs.containsKey(remoteSongs.get(i))) {
				Song song = localSongs.get(remoteSongs.get(i));
				remoteSongs.get(i).setLocation(song.getLocation());
			} else {
				logger.error("Could not find song: " + remoteSongs.get(i));
				throw new IllegalArgumentException("Could not find song: " + remoteSongs.get(i));
			}
		}
	}
}