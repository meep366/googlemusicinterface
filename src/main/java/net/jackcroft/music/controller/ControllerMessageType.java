package net.jackcroft.music.controller;

/**
 * Enum for messages to be passed to and from the controller interface
 * 
 * @author Jack Croft
 * 
 */
public enum ControllerMessageType {
	START, GOOGLE_LOGIN, PLEX_LOGIN, EXIT, ITUNES_PLAYLISTS_FROM_GOOGLE, ITUNES_PLAYLISTS_FROM_PLEX, PLEX_PLAYLIST_FROM_ITUNES;
}