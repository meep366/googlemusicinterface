package net.jackcroft.music.controller;

/**
 * Interface defining basic controller methods
 * 
 * @author Jack Croft
 * 
 */
public interface Controller {

	/**
	 * Method for sending a change of state message to the controller
	 * 
	 * @param updateMessage
	 *            The message for updating the controller
	 */
	public void update(ControllerMessageType updateMessage);

	/**
	 * Method for sending a response to the controller after an action was
	 * completed
	 * 
	 * @param reply
	 *            The reply from the result of the action
	 */
	public void response(ControllerMessageType reply);

	/**
	 * Method for starting the controller
	 */
	public void start();
}