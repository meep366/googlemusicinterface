package net.jackcroft.music.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.jackcroft.fileIO.FileWriter;
import net.jackcroft.music.MusicDatabase;
import net.jackcroft.music.MusicDatabaseComparator;
import net.jackcroft.music.MusicFileScanner;
import net.jackcroft.music.Playlist;
import net.jackcroft.music.User;
import net.jackcroft.music.view.AccountType;
import net.jackcroft.music.view.MusicDatabaseView;
import net.jackcroft.process.ProcRunner;
import net.jackcroft.process.StreamReader;
import net.jackcroft.text.GoogleParser;
import net.jackcroft.text.ItunesParser;
import net.jackcroft.text.PlexParser;

/**
 * Main controller for the application, can use either a command window view or
 * a GUI view
 * 
 * @author Jack Croft
 * 
 */
public class MusicDatabaseController implements Controller {
	private static Logger logger = LoggerFactory.getLogger(MusicDatabaseController.class);
	private static final double NANO_SECONDS = 1000000000;

	private MusicDatabaseView view;
	private User user;

	/**
	 * Constructor for the controller, requiring the view
	 * 
	 * @param v
	 *            The view for the controller, either GUI or command line
	 */
	public MusicDatabaseController(MusicDatabaseView v) {
		view = v;
		view.setController(this);
	}

	@Override
	public void update(ControllerMessageType updateMessage) {
		if (updateMessage == ControllerMessageType.START) {
			long startTime = System.nanoTime();
			user = view.getUser();
			if (view.getRememberInfo()) {
				Map<String, String> userProperties = new LinkedHashMap<>();
				userProperties.put("Directory:", user.getDirectory());
				writeInfoToFile(userProperties);
			}
			logger.info("Save User Info Time: " + (System.nanoTime() - startTime) / NANO_SECONDS);
		}
		if (updateMessage == ControllerMessageType.GOOGLE_LOGIN) {
			long startTime = System.nanoTime();
			try {
				user = view.getUser();
				if (view.getRememberInfo()) {
					Map<String, String> userProperties = new LinkedHashMap<>();
					userProperties.put("Directory:", user.getDirectory());
					writeInfoToFile(userProperties);
				}
				String filePath = writeFileToTempDirectory("src/main/python/", "Login.py");
				writeFileToTempDirectory("src/main/resources/", "mobileclient.cred");

				ProcRunner runner = new ProcRunner("python " + filePath);
				runner.start();
				runner.join();
				StreamReader reader = new StreamReader(runner.getOutputStream());
				reader.start();
				reader.join();
				boolean success = Boolean.valueOf(reader.getOutput());
				logger.info("Google Login Time: " + (System.nanoTime() - startTime) / NANO_SECONDS);
				view.login(AccountType.GOOGLE, success);
			} catch (IOException e) {
				logger.error(e.getMessage());
			} catch (InterruptedException ie) {
				logger.error(ie.getMessage());
			}
		} else if (updateMessage == ControllerMessageType.PLEX_LOGIN) {
			long startTime = System.nanoTime();
			try {
				user = view.getUser();
				if (view.getRememberInfo()) {
					Map<String, String> userProperties = new LinkedHashMap<>();
					userProperties.put("Directory:", user.getDirectory());
					userProperties.put("Plex Username:", user.getPlexUsername());
					userProperties.put("Plex Password:", user.getPlexPassword());
					writeInfoToFile(userProperties);
				}
				String filePath = writeFileToTempDirectory("src/main/python/", "PlexLogin.py");

				ProcRunner runner = new ProcRunner(
						"python " + filePath + " " + user.getPlexUsername() + " " + user.getPlexPassword());
				runner.start();
				runner.join();
				StreamReader reader = new StreamReader(runner.getOutputStream());
				StreamReader errorReader = new StreamReader(runner.getErrorStream());
				reader.start();
				errorReader.start();
				reader.join();
				errorReader.join();
				boolean success = reader.getOutput().equalsIgnoreCase("Success");
				if (!success) {
					logger.warn(errorReader.getOutput());
				}
				logger.info("Plex Login Time: " + (System.nanoTime() - startTime) / NANO_SECONDS);
				view.login(AccountType.PLEX, success);
			} catch (IOException e) {
				logger.error(e.getMessage());
			} catch (InterruptedException ie) {
				logger.error(ie.getMessage());
			}
		} else if (updateMessage == ControllerMessageType.ITUNES_PLAYLISTS_FROM_GOOGLE) {
			long startTime = System.nanoTime();
			try {
				String filePath = writeFileToTempDirectory("src/main/python/", "downloadAllScript.py");
				String musicDirectory = user.getDirectory();

				ProcRunner runner = new ProcRunner("python " + filePath);
				runner.start();
				runner.join();
				GoogleParser parser = new GoogleParser(runner.getOutputStream());
				parser.start();
				MusicFileScanner mfs = new MusicFileScanner(musicDirectory);
				mfs.start();
				mfs.join();
				parser.join();
				MusicDatabase googleMusic = parser.getDatabase();
				MusicDatabase localMusic = mfs.getDatabase();
				MusicDatabaseComparator comparator = new MusicDatabaseComparator();
				comparator.matchSongs(googleMusic, localMusic);
				List<Playlist> playlists = googleMusic.getPlaylists();
				FileWriter writer = new FileWriter();
				writer.writeAllPlaylists(playlists);
				logger.info(
						"Create iTunes Playlists from Google Time: " + (System.nanoTime() - startTime) / NANO_SECONDS);
				view.command("Playlists Created from Google");
			} catch (IOException ioe) {
				logger.error(ioe.getMessage());
			} catch (InterruptedException ie) {
				logger.error(ie.getMessage());
			}
		} else if (updateMessage == ControllerMessageType.ITUNES_PLAYLISTS_FROM_PLEX) {
			long startTime = System.nanoTime();
			try {
				String command = "python " + "src/main/python/PlexPlaylists.py " + user.getPlexUsername() + " "
						+ user.getPlexPassword();
				String musicDirectory = user.getDirectory();

				ProcRunner runner = new ProcRunner(command);
				runner.start();
				runner.join();
				StreamReader reader = new StreamReader(runner.getOutputStream());
				reader.start();
				reader.join();
				boolean success = reader.getOutput().equalsIgnoreCase("Success");
				if (!success) {
					logger.error("Failed to read Plex library");
					throw new RuntimeException("Failed to read Plex library");
				}
				File playlistFile = new File("playlists.txt");
				File songFile = new File("songs.txt");
				PlexParser parser = new PlexParser(playlistFile, songFile);
				parser.start();
				MusicFileScanner musicFileScanner = new MusicFileScanner(musicDirectory);
				musicFileScanner.start();
				musicFileScanner.join();
				parser.join();
				playlistFile.delete();
				songFile.delete();

				MusicDatabase plexMusic = parser.getDatabase();
				MusicDatabase localMusic = musicFileScanner.getDatabase();
				MusicDatabaseComparator comparator = new MusicDatabaseComparator();
				comparator.matchSongs(plexMusic, localMusic);
				List<Playlist> playlists = plexMusic.getPlaylists();
				FileWriter writer = new FileWriter();
				writer.writeAllPlaylists(playlists);
				logger.info(
						"Create iTunes Playlists from Plex Time: " + (System.nanoTime() - startTime) / NANO_SECONDS);
				view.command("Playlists Created from Plex");
			} catch (InterruptedException ie) {
				logger.error(ie.getMessage());
			}
		} else if (updateMessage == ControllerMessageType.PLEX_PLAYLIST_FROM_ITUNES) {
			long startTime = System.nanoTime();
			try {
				File playlistFile = new File(user.getPlexPlaylistFile());
				ItunesParser parser = new ItunesParser(playlistFile);
				parser.start();
				parser.join();

				MusicDatabase localMusic = parser.getDatabase();
				FileWriter writer = new FileWriter();
				Playlist playlist = localMusic.getPlaylists().get(0);
				if (view.isSorted()) {
					playlist.sortSongs();
				}
				File playlistJsonFile = writer.writePlexPlaylistJson(playlist);
				String command = "python " + "src/main/python/CreatePlexPlaylist.py " + user.getPlexUsername() + " "
						+ user.getPlexPassword() + " \"" + playlist.getName() + "\" \"" + playlistJsonFile.getAbsolutePath() + "\"";

				ProcRunner runner = new ProcRunner(command);
				runner.start();
				runner.join();
				StreamReader reader = new StreamReader(runner.getOutputStream());
				StreamReader errorReader = new StreamReader(runner.getErrorStream());
				reader.start();
				errorReader.start();
				reader.join();
				errorReader.join();
				boolean success = reader.getOutput().equalsIgnoreCase("Success");
				if (!success) {
					logger.error("Failed to create Plex playlist\n" + errorReader.getOutput());
					view.command("Failed to create Plex playlist\n" + errorReader.getOutput());
				} else {
					logger.info(
							"Create Plex Playlist from iTunes Time: " + (System.nanoTime() - startTime) / NANO_SECONDS);
					view.command("Playlist Created from iTunes");
					Files.deleteIfExists(playlistJsonFile.toPath());
				}
			} catch (InterruptedException ie) {
				logger.error(ie.getMessage());
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		} else if (updateMessage == ControllerMessageType.EXIT) {
			view.destroy();
		}
	}

	@Override
	public void response(ControllerMessageType reply) {
		// TODO Auto-generated method stub
	}

	@Override
	public void start() {
		String home = System.getProperty("user.home");
		File homeDirectory = new File(home);
		File directory = new File(home + "\\.GoogleMusicInterface");
		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.equals(".GoogleMusicInterface");
			}
		};
		if (homeDirectory.listFiles(filter).length == 0) {
			directory.mkdir();
			File info = new File(directory.getPath() + "\\info");
			String toWrite = "Directory:none";
			try {
				FileOutputStream fos = new FileOutputStream(info);
				fos.write(toWrite.getBytes());
				fos.close();
			} catch (IOException ioe) {
				logger.error(ioe.getMessage());
			}
		} else {
			File info = new File(directory.getPath() + "\\info");
			try {
				Scanner infoScanner = new Scanner(info);
				String musicDirectory = infoScanner.nextLine().substring(10);
				if (infoScanner.hasNextLine()) {
					String plexUsername = infoScanner.nextLine().substring(14);
					String plexPassword = infoScanner.nextLine().substring(14);
					user = new User(musicDirectory, plexUsername, plexPassword);
				} else {
					user = new User(musicDirectory);
				}
				infoScanner.close();
			} catch (FileNotFoundException fe) {
				logger.error(fe.getMessage());
			}
		}
		view.show();
	}

	/**
	 * Gets the user for the controller
	 * 
	 * @return the user for the controller
	 */
	public User getUser() {
		return user;
	}

	private void writeInfoToFile(Map<String, String> properties) {
		logger.debug("Writing user properties to file");
		String home = System.getProperty("user.home");
		File info = new File(home + "/.GoogleMusicInterface/info");
		info.delete();
		String newLine = System.getProperty("line.separator");
		String toWrite = "";
		for (Entry<String, String> entry : properties.entrySet()) {
			toWrite += entry.getKey() + entry.getValue() + newLine;
		}
		try {
			FileOutputStream fos = new FileOutputStream(info);
			fos.write(toWrite.getBytes());
			fos.close();
		} catch (IOException ioe) {
			logger.error(ioe.getMessage());
		}
	}

	private String writeFileToTempDirectory(String directory, String fileName) throws IOException {
		InputStream executableInputStream = new FileInputStream(new File(directory + fileName));
		File executableFile = new File(System.getProperty("java.io.tmpdir") + fileName);
		FileOutputStream outputStream = new FileOutputStream(executableFile);

		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = executableInputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		executableInputStream.close();
		outputStream.close();
		return executableFile.getAbsolutePath();
	}
}