package net.jackcroft.music;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class responsible for representing a song imported from an iTunes playlist.
 * 
 * @author Jack Croft
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ITunesSong {
	private static Logger logger = LoggerFactory.getLogger(ITunesSong.class);
	private String name;
	private Artist artist;
	private Album album;
	private int year;
	private LocalDateTime dateAdded;
	private int trackNumber;
	private int diskNumber;
	private List<String> genres;
	private String location;

	/**
	 * Constructor for an iTunes song from the CSV input.
	 * 
	 * @param name
	 *            The name of the song
	 * @param artist
	 *            The artist for the song
	 * @param album
	 *            The album for the song
	 * @param year
	 *            The year for the song
	 * @param dateAdded
	 *            The date the song was added
	 * @param trackNumber
	 *            The track number for the song
	 * @param diskNumber
	 *            The disk number for the song
	 * @param genre
	 *            The genre for the song
	 * @param location
	 *            The location for the song
	 */
	public ITunesSong(@JsonProperty("Name") String name, @JsonProperty("Artist") String artist,
			@JsonProperty("Album") String album, @JsonProperty("Year") int year,
			@JsonProperty("Date Added") String dateAdded, @JsonProperty("Track Number") int trackNumber,
			@JsonProperty("Disc Number") int diskNumber, @JsonProperty("Genre") String genre,
			@JsonProperty("Location") String location) {
		this.name = name;
		this.artist = new Artist(artist);
		try {
			AudioFile audioFile = AudioFileIO.read(new File(location));
			String albumArtist = audioFile.getTag().getFirst(FieldKey.ALBUM_ARTIST);
			if (albumArtist != null && !albumArtist.equals("")) {
				this.album = new Album(album, albumArtist);
			} else {
				this.album = new Album(album, artist);
			}
		} catch (Exception e) {
			logger.warn(e.getMessage());
			this.album = new Album(album, artist);
		}

		this.year = year;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy h:mm a");
		this.dateAdded = LocalDateTime.parse(dateAdded, formatter);
		this.trackNumber = trackNumber;
		this.diskNumber = diskNumber;
		this.genres = Arrays.asList(genre);
		this.location = location;
	}

	/**
	 * Creates a new Song from this iTunesSong
	 * 
	 * @return An equivalent Song to this iTunesSong
	 */
	public Song createSong() {
		long timestamp = ZonedDateTime.of(dateAdded, ZoneId.of("America/New_York")).toInstant().toEpochMilli();
		Song song = new Song(name, artist, album, year, timestamp, trackNumber, diskNumber, genres, location);
		return song;
	}
}
