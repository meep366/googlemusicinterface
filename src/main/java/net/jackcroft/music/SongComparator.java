package net.jackcroft.music;

import java.util.Comparator;

/**
 * A comparator for Songs, takes an int arg to represent the type of comparison
 * 
 * @author Jack Croft
 */
public class SongComparator implements Comparator<Song> {

	private int compareType;

	/**
	 * Constructor for the comparator, requires an int 0-18 that corresponds to
	 * a compare type, as listed in the song class
	 * 
	 * @param compareType
	 *            The compare type variable
	 */
	public SongComparator(int ct) {
		if (ct < 0 | ct > 18)
			throw new IllegalArgumentException();
		compareType = ct;
	}

	/**
	 * Changes the current compareType to a new one
	 * 
	 * @param ct
	 *            the new compareType
	 */
	public void setCompareType(int ct) {
		compareType = ct;
	}

	@Override
	public int compare(Song s1, Song s2) {
		if (compareType == 0)
			return s1.getLocation().compareTo(s2.getLocation());
		else if (compareType == 4)
			return s1.getYear() - s2.getYear();
		else if (compareType == 7) {
			if (s1.getAlbum().getName().equalsIgnoreCase(s2.getAlbum().getName()))
				return 0;
			return s1.getAlbum().compareTo(s2.getAlbum());
		} else if (compareType == 9) {
			if (s1.getTitle().equalsIgnoreCase(s2.getTitle()))
				return 0;
			return s1.getTitle().compareTo(s2.getTitle());
		} else if (compareType == 17) {
			if (s1.getArtist().equals(s2.getArtist()))
				return 0;
			return s1.getArtist().compareTo(s2.getArtist());
		} else if (compareType == 18)
			return (int) (s1.getDurationMilliseconds() - s2.getDurationMilliseconds());
		throw new UnsupportedOperationException();
	}
}