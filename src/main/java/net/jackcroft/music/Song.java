package net.jackcroft.music;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class that represents a song, with a bunch of instance variables for all the
 * data
 * 
 * @author Jack Croft
 */
public class Song implements Comparable<Song> {
	private String title = "";
	private Album album;
	private Artist artist;
	private String comment = "";
	private String composer = "";
	private int year;
	private long creationTimestamp;
	private String ID = "";
	private int trackNumber;
	private int diskNumber;
	private int beatsPerMinute;
	private List<String> genres;
	private int playCount;
	private long durationMilliseconds;
	private EncodedFileLocation location;

	/**
	 * Constructor for the Song requiring a map of String to String fields and
	 * values respectively
	 * 
	 * @param songMap
	 */
	public Song(Map<String, String> songMap, File f) {
		Set<String> keys = songMap.keySet();
		Iterator<String> i = keys.iterator();
		if (f != null)
			location = new EncodedFileLocation(f);
		String albumName = "";
		String albumArtistName = "";
		int totalDiskCount = 0;

		while (i.hasNext()) {
			String key = i.next();

			if (key.equals("Title"))
				setTitle(songMap.get(key));
			else if (key.equals("Album"))
				albumName = songMap.get(key);
			else if (key.equals("Artist"))
				setArtist(songMap.get(key));
			else if (key.equals("Comment"))
				setComment(songMap.get(key));
			else if (key.equals("Composer"))
				setComposer(songMap.get(key));
			else if (key.equals("Year"))
				setYear(Integer.valueOf(songMap.get(key)));
			else if (key.equals("Creation Timestamp"))
				setCreationTimestamp(Long.valueOf(songMap.get(key)));
			else if (key.equals("ID"))
				setID(songMap.get(key));
			else if (key.equals("Total Disk Count"))
				totalDiskCount = Integer.valueOf(songMap.get(key));
			else if (key.equals("Title"))
				setTitle(songMap.get(key));
			else if (key.equals("Album Artist"))
				albumArtistName = songMap.get(key);
			else if (key.equals("Track Number"))
				setTrackNumber(Integer.valueOf(songMap.get(key)));
			else if (key.equals("Disk Number"))
				setDiskNumber(Integer.valueOf(songMap.get(key)));
			else if (key.equals("Beats Per Minute"))
				setBeatsPerMinute(Integer.valueOf(songMap.get(key)));
			else if (key.equals("Genre"))
				setGenres(Arrays.asList(songMap.get(key)));
			else if (key.equals("Play Count"))
				setPlayCount(Integer.valueOf(songMap.get(key)));
			else if (key.equals("Duration Milliseconds"))
				setDurationMilliseconds(Long.valueOf(songMap.get(key)));
			else
				throw new IllegalArgumentException(key);
		}
		if (!albumArtistName.equals("")) {
			setArtist(albumArtistName);
		}
		if (!(albumName.equals("")))
			setAlbum(albumName, albumArtistName, totalDiskCount);
	}

	public Song(String title, Artist artist, Album album, int year, long dateAdded, int trackNumber, int diskNumber,
			List<String> genres, String location) {
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.year = year;
		this.creationTimestamp = dateAdded;
		this.trackNumber = trackNumber;
		this.diskNumber = diskNumber;
		this.genres = genres;
		this.location = new EncodedFileLocation(new File(location));
	}

	/**
	 * Constructor for the Song from Plex Json
	 * 
	 * @param title
	 *            Title for the song
	 * @param album
	 *            Album for the song
	 * @param artist
	 *            Artist for the song
	 * @param year
	 *            Year for the song
	 * @param creationTimestamp
	 *            Creation Timestamp for the song
	 * @param ID
	 *            ID for the song
	 * @param trackNumber
	 *            Track number for the song
	 * @param diskNumber
	 *            Disk number for the song
	 * @param genres
	 *            Genres for the song
	 * @param durationMilliseconds
	 *            Length for the song
	 */
	public Song(@JsonProperty("title") String title, @JsonProperty("album") Album album,
			@JsonProperty("artist") Artist artist, @JsonProperty("year") int year,
			@JsonProperty("creationTimestamp") String creationTimestamp, @JsonProperty("ID") String ID,
			@JsonProperty("trackNumber") int trackNumber, @JsonProperty("diskNumber") int diskNumber,
			@JsonProperty("genres") String[] genres, @JsonProperty("durationMilliseconds") long durationMilliseconds) {
		this.title = title;
		this.album = album;
		this.artist = artist;
		this.year = year;
		LocalDateTime localDateTime = LocalDateTime.parse(creationTimestamp, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		this.creationTimestamp = localDateTime.atZone(ZoneId.of("UTC")).toInstant().toEpochMilli();
		this.ID = ID;
		this.trackNumber = trackNumber;
		this.diskNumber = diskNumber;
		this.genres = Arrays.asList(genres);
		this.durationMilliseconds = durationMilliseconds;
	}

	/**
	 * Constructor for a song not requiring an initial file
	 * 
	 * @param songMap
	 */
	public Song(Map<String, String> songMap) {
		this(songMap, null);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Song))
			return false;
		Song otherSong = ((Song) o);

		boolean[] equals = { true, true, true };

		if (!title.equals("") || !otherSong.title.equals(""))
			equals[0] = title.equalsIgnoreCase(otherSong.title);
		if (album != null) {
			if (otherSong.album == null)
				return false;
			equals[1] = album.getName().equalsIgnoreCase(otherSong.album.getName());
		}
		if (artist != null) {
			if (otherSong.artist == null)
				return false;
			equals[2] = artist.getName().equalsIgnoreCase(otherSong.artist.getName());
		}

		return equals[0] && equals[1] && equals[2];
	}

	@Override
	public int hashCode() {
		return title.toLowerCase().hashCode() * album.hashCode() * artist.hashCode();
	}

	@Override
	public String toString() {
		Map<String, Object> attributes = new HashMap<String, Object>();
		if (!(title.equals("")))
			attributes.put("Title: ", title);
		if (artist != null)
			attributes.put("Artist: ", artist);
		if (album != null)
			attributes.put("Album: ", album);

		String print = "";
		Set<String> keys = attributes.keySet();
		Iterator<String> i = keys.iterator();
		while (i.hasNext()) {
			String next = i.next();
			print += next + attributes.get(next) + ", ";
		}
		if (print.length() > 0)
			print = print.substring(0, print.length() - 2);
		else
			print = "Blank Song";

		return print;
	}

	@Override
	public int compareTo(Song s) {
		return title.compareTo(s.title);
	}

	/**
	 * Returns the comparator of a Song using the title of the Artist.
	 * 
	 * @return The comparator of a Song using the title of the Artist
	 */
	public static Comparator<Song> getArtistComparator() {
		return new Comparator<Song>() {
			public int compare(Song songOne, Song songTwo) {
				String albumOneArtist = songOne.getArtist().getName().toLowerCase();
				String albumTwoArtist = songTwo.getArtist().getName().toLowerCase();
				if (albumOneArtist.length() > 4 && albumOneArtist.substring(0, 4).equalsIgnoreCase("The ")) {
					albumOneArtist = albumOneArtist.substring(4);
				}
				if (albumTwoArtist.length() > 4 && albumTwoArtist.substring(0, 4).equalsIgnoreCase("The ")) {
					albumTwoArtist = albumTwoArtist.substring(4);
				}
				if (albumOneArtist.length() > 2 && albumOneArtist.substring(0, 2).equalsIgnoreCase("A ")) {
					albumOneArtist = albumOneArtist.substring(2);
				}
				if (albumTwoArtist.length() > 2 && albumTwoArtist.substring(0, 2).equalsIgnoreCase("A ")) {
					albumTwoArtist = albumTwoArtist.substring(2);
				}
				return albumOneArtist.compareTo(albumTwoArtist);
			}
		};
	}

	/**
	 * Setter method for the location of the file
	 * 
	 * @param efl
	 *            the location of the file
	 */
	public void setLocation(EncodedFileLocation efl) {
		location = efl;
	}

	/**
	 * Getter method for the location
	 * 
	 * @return the song EncodedFileLocation
	 */
	public EncodedFileLocation getLocation() {
		return location;
	}

	/**
	 * Getter method for the song title
	 * 
	 * @return the song title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Getter method for the song Album
	 * 
	 * @return the song Album
	 */
	public Album getAlbum() {
		return album;
	}

	/**
	 * Getter method for the song Artist
	 * 
	 * @return the song Artist
	 */
	public Artist getArtist() {
		return artist;
	}

	/**
	 * Getter method for the song comment
	 * 
	 * @return the song comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Getter method for the song composer
	 * 
	 * @return the song composer
	 */
	public String getComposer() {
		return composer;
	}

	/**
	 * Getter method for the song year
	 * 
	 * @return the song year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * Getter method for the song creation timestamp
	 * 
	 * @return the song creation timestamp
	 */
	public long getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * Getter method for the song ID
	 * 
	 * @return the song ID
	 */
	public String getID() {
		return ID;
	}

	/**
	 * Getter method for the song track number
	 * 
	 * @return the song track number
	 */
	public int getTrackNumber() {
		return trackNumber;
	}

	/**
	 * Getter method for the song disk number
	 * 
	 * @return the song disk number
	 */
	public int getDiskNumber() {
		return diskNumber;
	}

	/**
	 * Getter method for the song beats per minute
	 * 
	 * @return the song beats per minute
	 */
	public int getBeatsPerMinute() {
		return beatsPerMinute;
	}

	/**
	 * Getter method for the song genre
	 * 
	 * @return The song genre
	 */
	public List<String> getGenres() {
		return genres;
	}

	/**
	 * Getter method for the song play count
	 * 
	 * @return the song play count
	 */
	public int getPlayCount() {
		return playCount;
	}

	/**
	 * Getter method for the song duration in milliseconds
	 * 
	 * @return the song duration in milliseconds
	 */
	public long getDurationMilliseconds() {
		return durationMilliseconds;
	}

	/**
	 * Private setter for the comment variable
	 * 
	 * @param c
	 *            the comment
	 */
	private void setComment(String c) {
		if (c == null)
			throw new IllegalArgumentException();
		comment = c;
	}

	/**
	 * Private setter for the title variable
	 * 
	 * @param t
	 */
	private void setTitle(String t) {
		if (t == null)
			throw new IllegalArgumentException();
		title = t;
	}

	/**
	 * Private setter for the album variable
	 * 
	 * @param albumName
	 *            the name of the album
	 * @param albumArtist
	 *            the album artist
	 * @param totalDiskCount
	 *            the total disk count
	 */
	private void setAlbum(String albumName, String albumArtist, int totalDiskCount) {
		album = new Album(albumName, albumArtist, totalDiskCount);
	}

	/**
	 * Private setter for the artist variable
	 * 
	 * @param name
	 *            the name of the artist
	 */
	private void setArtist(String name) {
		artist = new Artist(name);
	}

	/**
	 * Private setter for the composer variable
	 * 
	 * @param c
	 *            the song composer
	 */
	private void setComposer(String c) {
		if (c == null)
			throw new IllegalArgumentException();
		composer = c;
	}

	/**
	 * Private setter for the year variable
	 * 
	 * @param y
	 *            the song year
	 */
	private void setYear(int y) {
		if (year < 0)
			throw new IllegalArgumentException();
		year = y;
	}

	/**
	 * Private setter for the creation timestamp
	 * 
	 * @param cts
	 *            the creation timestamp
	 */
	private void setCreationTimestamp(long cts) {
		if (cts < 0)
			throw new IllegalArgumentException();
		creationTimestamp = cts;
	}

	/**
	 * Private setter method for the ID
	 * 
	 * @param id
	 *            the song ID
	 */
	private void setID(String id) {
		if (id == null || id.equals(""))
			throw new IllegalArgumentException();
		ID = id;
	}

	/**
	 * Private setter method for the track number
	 * 
	 * @param tn
	 *            the song track number
	 */
	private void setTrackNumber(int tn) {
		if (tn < 0)
			throw new IllegalArgumentException();
		trackNumber = tn;
	}

	/**
	 * Private setter for the disk number
	 * 
	 * @param dn
	 *            the song disk number
	 */
	private void setDiskNumber(int dn) {
		if (dn < 0)
			throw new IllegalArgumentException();
		diskNumber = dn;
	}

	/**
	 * Private setter for the beats per minute
	 * 
	 * @param bmp
	 *            the song beats per minute
	 */
	private void setBeatsPerMinute(int bmp) {
		if (bmp < 0)
			throw new IllegalArgumentException();
		beatsPerMinute = bmp;
	}

	/**
	 * Private setter for the genres
	 * 
	 * @param g
	 *            The song genres
	 */
	private void setGenres(List<String> g) {
		if (g == null)
			throw new IllegalArgumentException();
		genres = g;
	}

	/**
	 * Private setter for the play count
	 * 
	 * @param pc
	 *            the song play count
	 */
	private void setPlayCount(int pc) {
		if (pc < 0)
			throw new IllegalArgumentException();
		playCount = pc;
	}

	/**
	 * Private setter for the song duration in milliseconds
	 * 
	 * @param dms
	 *            the song duration in milliseconds
	 */
	private void setDurationMilliseconds(long dms) {
		if (dms < 0)
			throw new IllegalArgumentException();
		durationMilliseconds = dms;
	}
}