package net.jackcroft.music;

import net.jackcroft.music.controller.MusicDatabaseController;
import net.jackcroft.music.view.MusicDatabaseCmdView;
import net.jackcroft.music.view.MusicDatabaseView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

/**
 * Main class for running the Music Interface
 * 
 * @author Jack Croft
 */
public class Main {

	private static Logger logger = LoggerFactory.getLogger(Main.class);

	/**
	 * Main method to run the program
	 * 
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		logger.info("Starting program");
		long startTime = System.nanoTime();

		try {
			LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
			JoranConfigurator c = new JoranConfigurator();
			c.setContext(context);
			context.reset();
			c.doConfigure(ClassLoader.getSystemResourceAsStream("logback.xml"));
			MusicDatabaseView view = new MusicDatabaseCmdView();
			MusicDatabaseController controller = new MusicDatabaseController(view);
			MusicDatabaseApp app = new MusicDatabaseApp(controller, view);
			app.start();
		} catch (JoranException je) {
			logger.error(je.getMessage());
		}
		logger.info("Total Run Time: " + (System.nanoTime() - startTime) / 1000000000.);
	}
}