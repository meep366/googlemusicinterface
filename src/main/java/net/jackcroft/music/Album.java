package net.jackcroft.music;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing an album of music
 * 
 * @author Jack Croft
 */
public class Album implements Comparable<Album> {
	private String name;
	private String albumArtist;
	private int totalDiskCount;

	/**
	 * Constructor with only the album name
	 * 
	 * @param n
	 *            the name of the album
	 */
	public Album(String n) {
		this(n, "", 0);
	}

	/**
	 * Constructor with album name and disk count
	 * 
	 * @param n
	 *            the name of the album
	 * @param tdc
	 *            the total disk count for the album
	 */
	public Album(String n, int tdc) {
		this(n, "", tdc);
	}

	/**
	 * Constructor with name and album artist
	 * 
	 * @param name
	 *            the name of the album
	 * @param albumArtist
	 *            the name of the album artist
	 */
	@JsonCreator
	public Album(@JsonProperty("title") String name, @JsonProperty("albumArtist") String albumArtist) {
		this(name, albumArtist, 0);
	}

	/**
	 * Constructor with all the private instance variable data
	 * 
	 * @param n
	 *            the name of the album
	 * @param aa
	 *            the name of the album artist
	 * @param tdc
	 *            the total disk count of the album
	 */
	public Album(String n, String aa, int tdc) {
		if (n == null || n.equals("") || aa == null || tdc < 0)
			throw new IllegalArgumentException();
		name = n;
		albumArtist = aa;
		totalDiskCount = tdc;
	}

	@Override
	public int compareTo(Album album) {
		return name.compareTo(album.name);
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Album))
			return false;
		Album album = (Album) other;
		return name.equals(album.name) && totalDiskCount == album.totalDiskCount
				&& albumArtist.equals(album.albumArtist);
	}

	@Override
	public int hashCode() {
		return name.toLowerCase().hashCode();
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * Getter method for the name of the album
	 * 
	 * @return the name of the album
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter method for the album artist
	 * 
	 * @return the album artist
	 */
	public String getAlbumArtist() {
		return albumArtist;
	}

	/**
	 * Getter method for the total disk count
	 * 
	 * @return the total disk count
	 */
	public int getTotalDiskCount() {
		return totalDiskCount;
	}
}