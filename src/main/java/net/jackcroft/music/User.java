package net.jackcroft.music;

/**
 * Class defining a user that is signed in to the system
 * 
 * @author Jack Croft
 */
public class User {
	private String musicDirectory;
	private String plexUsername;
	private String plexPassword;
	private String plexPlaylistFile;

	/**
	 * User constructor requiring directory.
	 * 
	 * @param musicDirectory
	 *            The music directory for the user
	 */
	public User(String musicDirectory) {
		this.musicDirectory = musicDirectory;
	}

	/**
	 * User constructor taking in all the user parameters
	 * 
	 * @param musicDirectory
	 *            The music directory for the user
	 * @param plexUsername
	 *            The Plex username for the user
	 * @param plexPassword
	 *            The Plex password for the user
	 */
	public User(String musicDirectory, String plexUsername, String plexPassword) {
		this.musicDirectory = musicDirectory;
		this.plexUsername = plexUsername;
		this.plexPassword = plexPassword;
	}

	/**
	 * Returns the directory of the current user
	 * 
	 * @return The directory of the current user
	 */
	public String getDirectory() {
		return musicDirectory;
	}

	/**
	 * Returns the Plex username of the current user
	 * 
	 * @return The Plex username of the current user
	 */
	public String getPlexUsername() {
		return plexUsername;
	}

	/**
	 * Returns the Plex password of the current user
	 * 
	 * @return The Plex password of the current user
	 */
	public String getPlexPassword() {
		return plexPassword;
	}

	/**
	 * Returns the Plex playlist file of the current user
	 * 
	 * @return The Plex playlist file of the current user
	 */
	public String getPlexPlaylistFile() {
		return plexPlaylistFile;
	}

	/**
	 * Sets the Plex playlist file of the current user
	 * 
	 * @param plexPlaylistFile
	 *            The Plex playlist file for the current user
	 */
	public void setPlexPlaylistFile(String plexPlaylistFile) {
		this.plexPlaylistFile = plexPlaylistFile;
	}

	/**
	 * Returns whether or not this user has Plex info populated
	 * 
	 * @return Whether or not this user has Plex info populated
	 */
	public boolean hasPlexInfo() {
		return plexUsername != null && plexPassword != null;
	}

	@Override
	public String toString() {
		return "Directory: " + musicDirectory + ", Plex username: " + plexUsername;
	}
}