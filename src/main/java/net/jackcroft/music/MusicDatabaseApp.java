package net.jackcroft.music;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.jackcroft.music.controller.MusicDatabaseController;
import net.jackcroft.music.view.MusicDatabaseView;

/**
 * Class for controlling startup and closing of the application
 * 
 * @author Jack Croft
 * 
 */
public class MusicDatabaseApp {

	private static Logger logger = LoggerFactory.getLogger(MusicDatabaseApp.class);
	private MusicDatabaseController controller;
	private MusicDatabaseView view;

	/**
	 * Constructor for the class, requiring the main controller and view
	 * 
	 * @param c
	 *            The main MusicDatabaseController for the app
	 * @param v
	 *            The main MusicDatabaseView for the app
	 */
	public MusicDatabaseApp(MusicDatabaseController c, MusicDatabaseView v) {
		controller = c;
		view = v;
	}

	/**
	 * Method for starting the application
	 */
	public void start() {
		controller.start();
	}
}