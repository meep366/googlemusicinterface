package net.jackcroft.music;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing a music artist
 * 
 * @author Jack Croft
 */
public class Artist implements Comparable<Artist> {
	private String name;

	/**
	 * Constructor for artist with given name
	 * 
	 * @param name
	 *            The name of the artist
	 */
	@JsonCreator
	public Artist(@JsonProperty("title") String name) {
		if (name == null || name.equals(""))
			throw new IllegalArgumentException("Name cannot be blank");

		this.name = name;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Artist))
			return false;
		return name.equals(((Artist) other).name);
	}

	@Override
	public int hashCode() {
		return name.toLowerCase().hashCode();
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Artist artist) {
		return name.compareTo(artist.name);
	}

	/**
	 * Getter method for the name of the artist
	 * 
	 * @return the name of the artist
	 */
	public String getName() {
		return name;
	}
}