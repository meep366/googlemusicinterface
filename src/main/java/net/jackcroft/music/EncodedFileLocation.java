package net.jackcroft.music;

import java.io.File;

public class EncodedFileLocation implements Comparable<EncodedFileLocation> {
	private File file;

	/**
	 * Class constructor requiring a file
	 * 
	 * @param f
	 *            the file
	 */
	public EncodedFileLocation(File f) {
		if (f == null)
			throw new IllegalArgumentException();
		file = f;
	}

	/**
	 * Returns the file for the class
	 * 
	 * @return the file for the class
	 */
	public File getFile() {
		return file;
	}

	@Override
	public int compareTo(EncodedFileLocation efl) {
		return file.compareTo(efl.getFile());
	}
}