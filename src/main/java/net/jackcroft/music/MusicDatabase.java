package net.jackcroft.music;

import java.util.List;

/**
 * Class that represents a database of music, with related songs and playlists
 * 
 * @author Jack Croft
 */
public class MusicDatabase {

	private List<Song> songs;
	private List<Playlist> playlists;

	/**
	 * Constructor for a MusicDatabase, requiring a list of songs and playlists
	 * 
	 * @param p
	 *            the list of playlists
	 * @param s
	 *            the list of songs
	 */
	public MusicDatabase(List<Song> s, List<Playlist> p) {
		if (s == null)
			throw new IllegalArgumentException();
		playlists = p;
		songs = s;
	}

	/**
	 * Constructor requiring only a list of songs
	 * 
	 * @param s
	 *            the list of songs for the MusicDatabase
	 */
	public MusicDatabase(List<Song> s) {
		this(s, null);
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof MusicDatabase))
			return false;
		return songs.equals(((MusicDatabase) (other)).songs) && playlists.equals(((MusicDatabase) (other)).playlists);
	}

	@Override
	public int hashCode() {
		return songs.hashCode() + playlists.hashCode();
	}

	/**
	 * Getter method for the songs in the database
	 * 
	 * @return the songs in the database
	 */
	public List<Song> getSongs() {
		return songs;
	}

	/**
	 * Getter method for the playlists in the database
	 * 
	 * @return the playlists in the database
	 */
	public List<Playlist> getPlaylists() {
		return playlists;
	}
}