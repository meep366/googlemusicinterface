package net.jackcroft.music;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Data object representing a Plex Playlist.
 * 
 * @author Jack Croft
 *
 */
public class PlexPlaylist {
	private String title;
	private String[] tracks;

	/**
	 * Constructor for a PlexPlaylist taking in the title of the playlist and a
	 * list of tracks.
	 * 
	 * @param title
	 *            The title of the playlist
	 * @param tracks
	 *            The tracks in the playlist, consisting of unique id strings
	 */
	@JsonCreator
	public PlexPlaylist(@JsonProperty("title") String title, @JsonProperty("tracks") String[] tracks) {
		this.title = title;
		this.tracks = tracks;
	}

	/**
	 * Getter for the playlist title.
	 * 
	 * @return The playlist title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Getter for the playlist tracks.
	 * 
	 * @return The playlist tracks
	 */
	public String[] getTracks() {
		return tracks;
	}

	@Override
	public String toString() {
		return "Plex playlist " + title;
	}
}
