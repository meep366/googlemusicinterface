package net.jackcroft.music;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A class that represents a playlist, with a name, creation time, id, and a
 * list of songs
 * 
 * @author Jack Croft
 */
public class Playlist implements Comparable<Playlist> {
	private String name = "";
	private long creationTimestamp;
	private String ID = "";
	private List<Song> songs = new ArrayList<Song>();

	/**
	 * Constructor for the Playlist requiring a attribute map
	 * 
	 * @param playlistMap
	 *            a map of string attributes and their respective values
	 */
	public Playlist(Map<String, String> playlistMap) {
		Set<String> keys = playlistMap.keySet();
		Iterator<String> i = keys.iterator();
		while (i.hasNext()) {
			String key = i.next();
			if (key.equals("Name"))
				setName(playlistMap.get(key));
			else if (key.equals("Creation Timestamp"))
				setCreationTimestamp(Long.valueOf(playlistMap.get(key)));
			else if (key.equals("ID"))
				setID(playlistMap.get(key));
			else
				throw new IllegalArgumentException(key);
		}
	}

	/**
	 * Constructor for Playlist that sets name/ID to ID.
	 * 
	 * @param ID
	 *            The ID for the playlist
	 * @param songs
	 *            The list of songs that belong in the playlist
	 */
	public Playlist(String ID, List<Song> songs) {
		this.ID = ID;
		this.name = ID;
		this.songs = songs;
		this.creationTimestamp = 0;
	}

	/**
	 * Method to add a song to the playlist
	 * 
	 * @param s
	 *            the song to add
	 */
	public void addSong(Song s) {
		songs.add(s);
	}

	/**
	 * Method to add a collection of songs to the playlist
	 * 
	 * @param s
	 *            the songs to be added
	 */
	public void addSongs(Collection<Song> s) {
		songs.addAll(s);
	}

	/**
	 * Returns the number of songs in the playlist
	 * 
	 * @return the number of songs in the playlist
	 */
	public int size() {
		return songs.size();
	}

	/**
	 * Returns a list of songs in the playlist
	 * 
	 * @return the list of playlist songs
	 */
	public List<Song> getAllSongs() {
		return songs;
	}

	/**
	 * Sorts the playlist by artist title.
	 */
	public void sortSongs() {
		songs.sort(Song.getArtistComparator());
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Playlist))
			return false;
		return ((Playlist) o).ID.equals(ID);
	}

	@Override
	public int hashCode() {
		return ID.hashCode();
	}

	@Override
	public String toString() {
		Map<String, Object> attributes = new HashMap<String, Object>();
		if (!(name.equals("")))
			attributes.put("Name: ", name);
		if (!(songs.size() == 0))
			attributes.put("Tracks: ", songs.size());

		String print = "";
		Set<String> keys = attributes.keySet();
		Iterator<String> i = keys.iterator();
		while (i.hasNext()) {
			String next = i.next();
			print += next + attributes.get(next) + ", ";
		}
		if (print.length() > 0)
			print = print.substring(0, print.length() - 2);
		else
			print = "Blank Playlist";

		return print;
	}

	@Override
	public int compareTo(Playlist p) {
		return ID.compareTo(p.ID);
	}

	/**
	 * Returns the name of the playlist
	 * 
	 * @return the playlist name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Private setter for the name of the playlist
	 * 
	 * @param n
	 *            the name of the playlist
	 */
	private void setName(String n) {
		if (name == null)
			throw new IllegalArgumentException();
		name = n;
	}

	/**
	 * Gets the creation time for the playlist
	 * 
	 * @return the playlist creation timestamp
	 */
	public long getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * Private setter for the creation time of the playlist
	 * 
	 * @param cts
	 *            the playlist creation timestamp
	 */
	private void setCreationTimestamp(long cts) {
		if (cts < 0)
			throw new IllegalArgumentException();
		creationTimestamp = cts;
	}

	/**
	 * Getter method for the playlist id
	 * 
	 * @return the playlist id
	 */
	public String getID() {
		return ID;
	}

	/**
	 * Private setter method for the playlist id
	 * 
	 * @param id
	 *            the playlist id
	 */
	private void setID(String id) {
		if (id == null || id.equals(""))
			throw new IllegalArgumentException();
		ID = id;
	}
}