package net.jackcroft.fileIO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.stream.Collectors;

import net.jackcroft.music.Playlist;
import net.jackcroft.music.Song;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class for writing a given playlist to an iTunes file
 * 
 * @author Jack Croft
 */
public class FileWriter {

	private static Logger logger = LoggerFactory.getLogger(FileWriter.class);

	/**
	 * Convenience method for writing a list of playlists
	 * 
	 * @param playlists
	 *            the playlists to be written to file
	 */
	public void writeAllPlaylists(List<Playlist> playlists) {
		for (int i = 0; i < playlists.size(); i++)
			writePlaylistToFile(playlists.get(i));
	}

	/**
	 * Method to write a playlist to an iTunes file
	 * 
	 * @param playlist
	 *            the playlist to write
	 */
	public void writePlaylistToFile(Playlist playlist) {
		logger.trace("Making Playlist: " + playlist);
		try {
			List<Song> songs = playlist.getAllSongs();
			File playlistFolder = new File("PlaylistOutput");
			playlistFolder.mkdir();
			File playlistFile = new File("PlaylistOutput\\" + playlist.getName() + ".txt");
			FileOutputStream output = new FileOutputStream(playlistFile);
			OutputStreamWriter outputWriter = new OutputStreamWriter(output, "UTF-16");
			String header = "Name	Artist	Composer	Album	Grouping	Genre	Size	Time	Disc Number	Disc Count	Track Number	Track Count	Year	Date Modified	Date Added	Bit Rate	Sample Rate	Volume Adjustment	Kind	Equalizer	Comments	Plays	Last Played	Skips	Last Skipped	My Rating	Location"
					+ "\r\n";
			outputWriter.write(header);

			for (int i = 0; i < songs.size(); i++) {
				File song = songs.get(i).getLocation().getFile();
				for (int j = 0; j < 26; j++)
					outputWriter.write('\t');
				outputWriter.write(song.getPath());
				outputWriter.write('\r');
				outputWriter.write('\n');
			}
			outputWriter.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Method to write out a JSON file containing all the playlist songs
	 * 
	 * @param playlist
	 *            The playlist to convert to JSON
	 */
	public File writePlexPlaylistJson(Playlist playlist) {
		logger.trace("Making Plex Playlist: " + playlist);
		try {
			List<Song> songs = playlist.getAllSongs();
			List<JsonSong> jsonSongs = songs.stream().map(JsonSong::new).collect(Collectors.toList());
			ObjectMapper jsonMapper = new ObjectMapper();
			String json = jsonMapper.writeValueAsString(jsonSongs);
			File jsonPlaylistFile = new File(playlist.getName() + ".json");
			FileOutputStream output = new FileOutputStream(jsonPlaylistFile);
			OutputStreamWriter outputWriter = new OutputStreamWriter(output, "UTF-8");
			outputWriter.write(json);
			outputWriter.close();
			return jsonPlaylistFile;
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}
}