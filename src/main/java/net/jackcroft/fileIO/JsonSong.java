package net.jackcroft.fileIO;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.jackcroft.music.Song;

/**
 * Class used to write out JSON file format for Song to be read by Plex python
 * script
 * 
 * @author Jack Croft
 *
 */
public class JsonSong {
	@JsonProperty("title")
	private String title;

	@JsonProperty("artist")
	private String artist;

	@JsonProperty("album")
	private String album;

	@JsonProperty("trackNumber")
	private String trackNumber;

	/**
	 * Constructor from a Song
	 * 
	 * @param song
	 *            The Song to build the JsonSong from
	 */
	public JsonSong(Song song) {
		title = song.getTitle();
		artist = song.getAlbum().getAlbumArtist();
		album = song.getAlbum().getName();
		trackNumber = String.valueOf(song.getTrackNumber());
	}

	/**
	 * Returns the title for the JsonSong
	 * 
	 * @return The title for the JsonSong
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the artist for the JsonSong
	 * 
	 * @return The artist for the JsonSong
	 */
	public String getArtist() {
		return artist;
	}

	/**
	 * Returns the album for the JsonSong
	 * 
	 * @return The album for the JsonSong
	 */
	public String getAlbum() {
		return album;
	}

	/**
	 * Returns the track number for the JsonSong
	 *
	 * @return The track number for the JsonSong
	 */
	public String getTrackNumber() {
		return trackNumber;
	}
}
