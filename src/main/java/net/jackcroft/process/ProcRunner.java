package net.jackcroft.process;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to run any given program and command
 * 
 * @author Jack Croft
 */
public class ProcRunner extends Thread {

	private static Logger logger = LoggerFactory.getLogger(ProcRunner.class);
	private String command;
	private InputStream output;
	private InputStream error;
	private OutputStream input;

	/**
	 * Constructor for the ProcRunner requiring an initial command
	 * 
	 * @param c
	 *            the command to be run
	 */
	public ProcRunner(String c) {
		command = c;
	}

	@Override
	public void run() {
		logger.info("Executing command: " + command);
		if (command.equals(""))
			throw new IllegalArgumentException("Invalid Command Argument");
		try {
			Runtime rt = Runtime.getRuntime();
			Process process = rt.exec(command);
			output = process.getInputStream();
			error = process.getErrorStream();
			input = process.getOutputStream();
		} catch (IOException ioe) {
			logger.error(ioe.getMessage());
		}
	}

	/**
	 * Returns the process output stream
	 * 
	 * @return the process output stream
	 */
	public InputStream getOutputStream() {
		return output;
	}

	/**
	 * Returns the process error stream
	 * 
	 * @return the error stream of the process
	 */
	public InputStream getErrorStream() {
		return error;
	}

	/**
	 * Returns the input stream for the process
	 * 
	 * @return the process input stream
	 */
	public OutputStream getInputStream() {
		return input;
	}
}