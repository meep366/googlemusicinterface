package net.jackcroft.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.IntBuffer;
import java.util.Arrays;

/**
 * Thread class to get the output from the given process
 * 
 * @author Jack Croft
 */
public class StreamReader extends Thread {

	private InputStream input;
	private String output = "";

	/**
	 * Constructor requiring InputStream to be read
	 * 
	 * @param is
	 *            the InputStream to be read
	 */
	public StreamReader(InputStream is) {
		input = is;
	}

	/**
	 * Returns the output from process
	 * 
	 * @return the output from process
	 */
	public String getOutput() {
		return output.trim();
	}

	@Override
	public void run() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(input, "Cp1252"));
			IntBuffer c = IntBuffer.allocate(100000000);
			boolean done = false;
			while (!done) {
				int input = reader.read();

				if (input != -1)
					c.put(input);
				else
					done = true;
			}
			int[] ints = c.array();
			ints = Arrays.copyOfRange(ints, 0, ints.length - c.position());
			output = new String(ints, 0, ints.length);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}