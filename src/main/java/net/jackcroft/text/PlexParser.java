package net.jackcroft.text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.jackcroft.music.MusicDatabase;
import net.jackcroft.music.Playlist;
import net.jackcroft.music.PlexPlaylist;
import net.jackcroft.music.Song;

public class PlexParser extends Thread {
	private static Logger logger = LoggerFactory.getLogger(PlexParser.class);
	private File playlistFile;
	private File songFile;
	private boolean done;
	private MusicDatabase database;

	/**
	 * Constructor for Parser requiring an InputStream of data to parse
	 * 
	 * @param is
	 */
	public PlexParser(File playlistFile, File songFile) {
		this.playlistFile = playlistFile;
		this.songFile = songFile;
		done = false;
		database = null;
	}

	/**
	 * Returns whether or not the class is done parsing
	 * 
	 * @return whether or not the class is done parsing
	 */
	public boolean isDone() {
		return done;
	}

	@Override
	public void run() {
		logger.info("Parsing Python Input");
		long startTime = System.nanoTime();
		try {
			InputStream songInputStream = new FileInputStream(songFile);
			InputStream playlistInputStream = new FileInputStream(playlistFile);
			ObjectMapper objectMapper = new ObjectMapper();
			Song[] songArray = objectMapper.readValue(songInputStream, Song[].class);
			PlexPlaylist[] playlistArray = objectMapper.readValue(playlistInputStream, PlexPlaylist[].class);
			logger.debug("Read Json Time: " + (System.nanoTime() - startTime) / 1000000000.);

			List<Song> songs = Arrays.asList(songArray);
			List<Playlist> playlists = new ArrayList<>();
			Map<String, Song> songIdMap = createSongIdMap(songs);
			for (PlexPlaylist plexPlaylist : playlistArray) {
				playlists.add(convertPlexPlaylistToPlaylist(plexPlaylist, songIdMap));
			}

			database = new MusicDatabase(songs, playlists);
			done = true;
			logger.debug("Total Parse Time: " + (System.nanoTime() - startTime) / 1000000000.);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	private Map<String, Song> createSongIdMap(List<Song> songs) {
		Map<String, Song> songIdMap = new HashMap<>();
		for (Song song : songs) {
			songIdMap.put(song.getID(), song);
		}
		return songIdMap;
	}

	private Playlist convertPlexPlaylistToPlaylist(PlexPlaylist plexPlaylist, Map<String, Song> songIdMap) {
		logger.trace("Parsing Plex Playlist" + plexPlaylist);

		List<Song> playlistSongs = new ArrayList<>();
		for (String id : plexPlaylist.getTracks()) {
			playlistSongs.add(songIdMap.get(id));
		}

		return new Playlist(plexPlaylist.getTitle(), playlistSongs);
	}

	/**
	 * Getter method for the database
	 * 
	 * @return the MusicDatabase
	 */
	public MusicDatabase getDatabase() {
		return database;
	}
}
