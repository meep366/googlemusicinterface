package net.jackcroft.text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import net.jackcroft.music.MusicDatabase;
import net.jackcroft.music.Playlist;
import net.jackcroft.music.Song;
import net.jackcroft.music.ITunesSong;

/**
 * Class responsible for parsing an iTunes playlist in to Song and Playlist
 * classes.
 * 
 * @author Jack Croft
 *
 */
public class ItunesParser extends Thread {
	private static Logger logger = LoggerFactory.getLogger(ItunesParser.class);
	private File playlistFile;
	private boolean done;
	private MusicDatabase database;

	/**
	 * Constructor for Parser requiring an InputStream of data to parse
	 * 
	 * @param is
	 */
	public ItunesParser(File playlistFile) {
		this.playlistFile = playlistFile;
		done = false;
		database = null;
	}

	/**
	 * Returns whether or not the class is done parsing
	 * 
	 * @return whether or not the class is done parsing
	 */
	public boolean isDone() {
		return done;
	}

	@Override
	public void run() {
		logger.info("Parsing iTunes Input");
		long startTime = System.nanoTime();
		try {
			InputStream playlistInputStream = new FileInputStream(playlistFile);

			CsvMapper csvMapper = new CsvMapper();
			csvMapper.setSerializationInclusion(Include.NON_NULL);
			CsvSchema schema = csvMapper.typedSchemaFor(ITunesSong.class).withHeader().withColumnSeparator('\t')
					.withComments();
			MappingIterator<ITunesSong> dataIterator = csvMapper.readerFor(ITunesSong.class).with(schema)
					.readValues(playlistInputStream);
			List<ITunesSong> iTunesSongList = dataIterator.readAll();
			List<Song> songList = iTunesSongList.stream().map(ITunesSong::createSong).collect(Collectors.toList());
			logger.debug("Read iTunes Time: " + (System.nanoTime() - startTime) / 1000000000.);

			Playlist playlist = new Playlist(playlistFile.getName().replace(".txt", ""), songList);
			List<Playlist> playlists = Arrays.asList(playlist);

			database = new MusicDatabase(songList, playlists);
			done = true;
			logger.debug("Total Parse Time: " + (System.nanoTime() - startTime) / 1000000000.);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Getter method for the database
	 * 
	 * @return the MusicDatabase
	 */
	public MusicDatabase getDatabase() {
		return database;
	}
}
