package net.jackcroft.text;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jackcroft.music.MusicDatabase;
import net.jackcroft.music.Playlist;
import net.jackcroft.music.Song;
import net.jackcroft.process.StreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for parsing input text and returning a MusicDatabase
 * 
 * @author Jack Croft
 */
public class GoogleParser extends Thread {

	private static Logger logger = LoggerFactory.getLogger(GoogleParser.class);
	private InputStream songInput;
	private boolean done;
	private MusicDatabase database;

	/**
	 * Constructor for Parser requiring an InputStream of data to parse
	 * 
	 * @param is
	 */
	public GoogleParser(InputStream is) {
		songInput = is;
		done = false;
		database = null;
	}

	/**
	 * Returns whether or not the class is done parsing
	 * 
	 * @return whether or not the class is done parsing
	 */
	public boolean isDone() {
		return done;
	}

	@Override
	public void run() {
		logger.info("Parsing Python Input");
		long startTime = System.nanoTime();
		StreamReader reader = new StreamReader(songInput);
		String fileString = "";
		String[] files = null;

		try {
			reader.start();
			reader.join();
			fileString = reader.getOutput();
		} catch (InterruptedException ie) {
			logger.error(ie.getMessage());
		}

		logger.debug("Read Python Time: " + (System.nanoTime() - startTime) / 1000000000.);

		files = fileString.split("\r\nNEW FILE\r\n");
		if (files.length < 2) {
			logger.error("Python returned no data");
			throw new RuntimeException("Python returned nothing");
		}
		List<Song> songs = parseSongs(files[1].split("\r\n"));
		List<Playlist> playlists = parsePlaylists(songs, files[0].split("\r\n"));

		database = new MusicDatabase(songs, playlists);
		done = true;
		logger.debug("Total Parse Time: " + (System.nanoTime() - startTime) / 1000000000.);
	}

	/**
	 * Getter method for the database
	 * 
	 * @return the MusicDatabase
	 */
	public MusicDatabase getDatabase() {
		return database;
	}

	/**
	 * Method for getting an attribute from a Python dictionary put into string
	 * form
	 * 
	 * @param map
	 *            the map to put the resulting attribute and value in
	 * @param line
	 *            the dictionary line to be parsed
	 * @param attribute
	 *            the attribute being looked for
	 * @param current
	 *            the first string to split the line with
	 * @param end
	 *            the second string to split the line with
	 * @param shift
	 *            the shift from the beginning of the split string to the
	 *            attribute
	 * @return the substring of the line, cut after the attribute
	 */
	private String parseAttribute(Map<String, String> map, String line, String attribute, String current, String end,
			int shift) {
		logger.trace("Adding song attribute: " + attribute);
		int currentIndex = line.indexOf(current) + shift;
		int endIndex = 0;
		endIndex = line.indexOf(end, currentIndex);
		if (currentIndex != shift - 1) {
			map.put(attribute, line.substring(currentIndex, endIndex));
			line = line.substring(endIndex);
		}
		return line;
	}

	/**
	 * A method taking in lines of Python output and creating a list of Songs
	 * 
	 * @param lineFile
	 *            the Python input, where each line is a song
	 * @return the list of Songs from the output
	 */
	public List<Song> parseSongs(String[] lineFile) {
		logger.info("Parsing Songs");
		long startTime = System.nanoTime();
		List<Song> songs = new ArrayList<Song>();
		ArrayList<String> temp = new ArrayList<String>();

		for (int i = 0; i < lineFile.length; i++) {
			if (!lineFile[i].equals(""))
				temp.add(lineFile[i]);
		}
		String[] lines = new String[temp.size()];
		temp.toArray(lines);
		String[][] songArrays = new String[lines.length][];

		for (int i = 0; i < lines.length; i++) {
			songArrays[i] = lines[i].split("\\{\\[\\(|\\)\\]\\}");
		}
		for (int i = 0; i < songArrays.length; i++) {
			Map<String, String> songMap = new HashMap<String, String>();
			for (int j = 0; j < songArrays[i].length; j++) {
				if (!(songArrays[i][j].equals("") || songArrays[i][j].matches("\\s")
						|| songArrays[i][j].equals("\r"))) {
					String[] breakUp = songArrays[i][j].split(":", 2);
					if (breakUp.length != 1) {
						try {
							breakUp[1] = new String(breakUp[1].getBytes(), "utf-8");
						} catch (UnsupportedEncodingException uee) {
							uee.printStackTrace();
						}
						songMap.put(breakUp[0], breakUp[1]);
					} else
						songMap.put(breakUp[0], "");
				}
			}
			Song s = new Song(songMap);
			songs.add(s);
		}
		logger.debug("Parse Songs Time: " + (System.nanoTime() - startTime) / 1000000000.);
		return songs;
	}

	/**
	 * A method taking in a list of songs and lines of Python output and
	 * creating a list of Playlists
	 * 
	 * @param songList
	 *            the list of songs, to be put in the Playlists
	 * @param fileLines
	 *            the Python output, where each line is a playlist
	 * @return the list of Playlists from the output
	 */
	public List<Playlist> parsePlaylists(List<Song> songList, String[] fileLines) {
		logger.info("Parsing playlists");
		long startTime = System.nanoTime();
		List<Playlist> playlists = new ArrayList<Playlist>();
		ArrayList<String> temp = new ArrayList<String>();
		for (int i = 0; i < fileLines.length; i++) {
			if (!fileLines[i].equals(""))
				temp.add(fileLines[i]);
		}
		String[] lines = new String[temp.size()];
		temp.toArray(lines);
		String[][] playlistArrays = new String[lines.length][];
		String[] tracks = new String[lines.length];
		for (int i = 0; i < lines.length; i++) {
			playlistArrays[i] = lines[i].split("\\{\\[\\(|\\)\\]\\}");
		}

		for (int i = 0; i < playlistArrays.length; i++) {
			Map<String, String> playlistMap = new HashMap<String, String>();
			for (int j = 0; j < playlistArrays[i].length; j++) {
				if (!(playlistArrays[i][j].equals("") || playlistArrays[i][j].equals(" ")
						|| playlistArrays[i][j].equals("\r"))) {
					if (!(playlistArrays[i][j].contains("Tracks:[{"))) {
						String[] breakUp = playlistArrays[i][j].split(":");
						if (breakUp.length != 1)
							playlistMap.put(breakUp[0], breakUp[1]);
						else
							playlistMap.put(breakUp[0], "");
					} else
						tracks[i] = playlistArrays[i][j];
				}
			}
			Playlist p = new Playlist(playlistMap);
			playlists.add(p);
		}
		parsePlaylistFileSongs(tracks, playlists, songList);
		logger.debug("Parse Playlists Time: " + (System.nanoTime() - startTime) / 1000000000.);
		return playlists;
	}

	/**
	 * A helper method to parse songs from a Python playlist track listing
	 * 
	 * @param songStrings
	 *            the list of strings from the Python output
	 * @param songList
	 *            the corresponding Song list for finding Songs
	 * @return the list of Songs that correspond with the Playlist
	 */
	private List<Song> parsePlaylistSongs(String[] songStrings, List<Song> songList) {
		logger.trace("Parsing Playlist Song");
		List<Song> songs = new ArrayList<Song>();
		Map<String, Song> songMap = new HashMap<String, Song>();
		for (int i = 0; i < songList.size(); i++)
			songMap.put(songList.get(i).getID(), songList.get(i));
		for (int i = 0; i < songStrings.length; i++) {
			Map<String, String> map = new HashMap<String, String>();
			parseAttribute(map, songStrings[i], "ID", "'trackId", "', '", 12);
			songs.add(songMap.get(map.get("ID")));
		}
		return songs;
	}

	/**
	 * A helper method to split up tracks from a Python playlist output
	 * 
	 * @param lines
	 *            the lines representing Python playlist output
	 * @param playlists
	 *            the Playlists that need Songs
	 * @param songList
	 *            the corresponding Song list to help find proper Songs for the
	 *            Playlists
	 */
	private void parsePlaylistFileSongs(String[] lines, List<Playlist> playlists, List<Song> songList) {
		logger.trace("Parsing All Playlist Songs");
		String[][] songs = new String[lines.length][];

		for (int i = 0; i < lines.length; i++)
			songs[i] = lines[i].split("(\\'tracks\\': \\[\\{)|(\\}, \\{)|(\\})|('\\}\\])");

		for (int i = 0; i < songs.length; i++)
			playlists.get(i).addSongs(parsePlaylistSongs(songs[i], songList));
	}
}