import sys
import json
from plexapi.myplex import MyPlexAccount
from plexapi.playlist import Playlist


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


email = sys.argv[1]
password = sys.argv[2]
playlistName = sys.argv[3]
playlistFileLocation = sys.argv[4]
account = MyPlexAccount(email, password)
plex = account.resource('HomeMediaServer').connect()
music = plex.library.section('Music')

with open(playlistFileLocation, encoding="utf-8") as playlistFile:
    playlistSongs = json.load(playlistFile)

songs = []
missingSongs = []
for song in playlistSongs:
    filters = {'artist.title': song['artist'], 'album.title': song['album'], 'track.title': song['title'], 'track.index': song['trackNumber']}
    tracks = music.search(filters=filters, libtype='track', maxresults=1)
    if len(tracks) == 0:
        missingSongs.append('Title: {}, Artist: {}, Album: {}, TrackNumber: {}'.format(song['title'], song['artist'], song['album'], song['trackNumber']))
    else:
        songs.append(tracks[0])

if len(missingSongs) == 0:
    newPlaylist = Playlist.create(plex, playlistName, items=songs, section=music)
    print('Success')
else:
    eprint('The following songs were not found')
    for song in missingSongs:
        eprint(song)
