import sys
import json
import datetime
from plexapi.myplex import MyPlexAccount
from plexapi.media import Genre

class Song:
    def __init__(self, title, artist, album, year, creationTimestamp, ID, trackNumber, diskNumber, genres, durationMilliseconds):
        self.title = title
        self.artist = artist
        self.album = album
        self.year = year
        self.creationTimestamp = creationTimestamp
        self.ID = ID
        self.trackNumber = trackNumber
        self.diskNumber = diskNumber
        self.genres = genres
        self.durationMilliseconds = durationMilliseconds
class Artist:
        def __init__(self, title):
            self.title = title
class Album:
        def __init__(self, title, albumArtist):
            self.title = title
            self.albumArtist = albumArtist
class Playlist:
                def __init__(self, title):
                    self.title = title
                    self.tracks = []
def serialize(obj):
    if isinstance(obj, datetime.date):
        serial = obj.isoformat()
        return serial

    if isinstance(obj, datetime.time):
        serial = obj.isoformat()
        return serial

    if isinstance(obj, Genre):
        serial = obj.tag
        return serial

    return obj.__dict__

email = sys.argv[1]
password = sys.argv[2]
account = MyPlexAccount(email, password)
plex = account.resource('HomeMediaServer').connect()
songs = []
playlists = []
for plexPlaylist in plex.playlists():
    if plexPlaylist.playlistType == 'audio':
        playlist = Playlist(plexPlaylist.title)
        for item in plexPlaylist.items():
            artist = Artist(item.artist().title)
            album = Album(item.album().title, item.album().parentTitle)
            song = Song(item.title, artist, album, item.album().year, item.addedAt, item.guid, item.index, item.parentIndex, item.album().genres, item.duration)
            songs.append(song)
            playlist.tracks.append(item.guid)
        playlists.append(playlist)
with open ('playlists.txt', 'w', encoding='utf8') as playlistFile:
    json.dump(playlists, playlistFile, default=serialize, ensure_ascii=False, indent=4)
with open ('songs.txt', 'w', encoding='utf8') as songsFile:
    json.dump(songs, songsFile, default=serialize, ensure_ascii=False, indent=4)

print('Success')
