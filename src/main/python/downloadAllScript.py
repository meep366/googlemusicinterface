'''
Script to read in data from Google Play Music and print to the console
@author: Jack Croft
'''
import time
import sys
from gmusicapi import Mobileclient

sys.stdout.reconfigure(encoding='utf-8')
ctime = time.perf_counter()
api = Mobileclient()
creds = "./src/main/resources/mobileclient.cred"
#creds = "mobileclient.cred"
api.oauth_login("69e4c210af7a35a7bac00f9f6f485eaead7e56c1a317c33553fbb5daf2a67ce3", creds)

playlistList = api.get_all_user_playlist_contents()
songList = api.get_all_songs()

for s in playlistList:
    output = "{[(Name:%s)]}{[(ID:%s)]}{[(Creation Timestamp:%s)]}{[(Tracks:%s)]}"%(s['name'], s['id'], s['creationTimestamp'], s['tracks'])
    sys.stdout.write(output + "\n")

sys.stdout.write("\nNEW FILE\n")

for s in songList:
    attributes = list()
    realAttributes = list()
    if("comment" in s):
        attributes.append("comment")
        realAttributes.append("Comment:")
    if("composer" in s):
        attributes.append("composer")
        realAttributes.append("Composer:")
    if("year" in s):
        attributes.append("year")
        realAttributes.append("Year:")
    if("diskNumber" in s):
        attributes.append("diskNumber")
        realAttributes.append("Disk Number:")
    if("beatsPerMinute" in s):
        attributes.append("beatsPerMinute")
        realAttributes.append("Beats Per Minute:")
    if("playCount" in s):
        attributes.append("playCount")
        realAttributes.append("Play Count:")
    output = "{[(Artist:%s)]}{[(Title:%s)]}{[(Album:%s)]}{[(Creation Timestamp:%s)]}{[(ID:%s)]}{[(Total Disk Count:%s)]}{[(Album Artist:%s)]}{[(Track Number:%s)]}{[(Genre:%s)]}{[(Duration Milliseconds:%s)]}" % (s['artist'], s['title'], s['album'], s['creationTimestamp'], s['id'], s['totalDiscCount'], s['albumArtist'], s['trackNumber'], s['genre'], s['durationMillis'])
    i = 0
    while(i < len(attributes)):
        output = output + "{[(" + realAttributes[i] + "%s)]}" % (s[attributes[i]])
        i = i + 1
    sys.stdout.write(output + "\n")

sys.stdout.write("\n" + "NEW FILE" + "\n") #keep this to mark end of file data
sys.stdout.write("Total Time: " + str(time.perf_counter() - ctime))
