Short Term Features:
Add support for creating Plex playlists
Add support for reading Plex playlists
Fix bug when entering in text before the login successful message comes up
Better Unit Testing
Sync metadata between songs on iTunes and Plex

Long Term Features:
Create GUI to supplement command line interface
Sync new music between libraries and add to relevant playlists
Help sync up libraries between different iTunes and Plex accounts

Setup project:
Make sure PyInstaller and GMusicApi are installed, using pip
PyInstaller uses Pywin32
To make sure protobuf can be used by the Python executables, add and compile blank __init__.py to google directory. See; http://stackoverflow.com/questions/13862562/google-protocol-buffers-not-found-when-trying-to-freeze-python-app